function getLogin(username, password) {
  var requestOptions = {
    method: "POST",
    redirect: "follow",
    credentials: "include",
    body: JSON.stringify({ username: username, password: password }),
  };

  return fetch(`${process.env.REACT_APP_API_URL}/lfpm/login`, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      return result;
    })
    .catch((error) => console.log("error", error));
}

function registerUser(username, password) {
  var requestOptions = {
    method: "POST",
    redirect: "follow",
    credentials: "include",
    body: JSON.stringify({ username: username, password: password }),
  };

  return fetch(`${process.env.REACT_APP_API_URL}/lfpm/users`, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      return result;
    })
    .catch((error) => console.log("error", error));
}

function getUser() {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
    credentials: "include",
  };

  return fetch(`${process.env.REACT_APP_API_URL}/lfpm/users`, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      return result;
    })
    .catch((error) => console.log("error", error));
}

function getFapsByUsername(username) {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
    credentials: "include",
  };

  return fetch(
    `${process.env.REACT_APP_API_URL}/lfpm/faps?username=${username}`,
    requestOptions
  )
    .then((response) => response.json())
    .then((result) => {
      return result;
    })
    .catch((error) => console.log("error", error));
}

function addFap(fapType, fapContent) {
  var requestOptions = {
    method: "POST",
    redirect: "follow",
    credentials: "include",
    body: JSON.stringify({
      fap_type: fapType,
      fap_content: fapContent,
    }),
  };

  return fetch(`${process.env.REACT_APP_API_URL}/lfpm/faps`, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      return result;
    })
    .catch((error) => console.log("error", error));
}

function getAllFaps(username) {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
    credentials: "include",
  };

  return fetch(`${process.env.REACT_APP_API_URL}/lfpm/faps/all`, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      return result;
    })
    .catch((error) => console.log("error", error));
}

module.exports = {
  getLogin,
  getUser,
  getFapsByUsername,
  registerUser,
  addFap,
  getAllFaps,
};
