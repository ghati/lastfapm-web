import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { CSSReset, ChakraProvider } from "@chakra-ui/core";
import * as serviceWorker from "./serviceWorker";
import theme from "./theme";

ReactDOM.render(
  <ChakraProvider theme={theme}>
    <CSSReset />
    <App />
  </ChakraProvider>,
  document.getElementById("root")
);

serviceWorker.unregister();
