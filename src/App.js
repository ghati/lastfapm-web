import React from "react";
import { Container } from "@chakra-ui/core";
import Header from "./components/Header";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Profile from "./pages/Profile";
import Home from "./pages/Home";
import Feed from "./pages/Feed";
import Add from "./pages/Add";
import LoadUser from "./components/LoadUser";

function App() {
  return (
    <div className="App">
      <LoadUser />
      <Router>
        <Header />
        <Container>
          <Switch>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/register">
              <Register />
            </Route>
            <Route path="/add">
              <Add />
            </Route>
            <Route path="/profile/:username" children={<Profile />}></Route>
            <Route path="/feed">
              <Feed />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Container>
      </Router>
    </div>
  );
}

export default App;
