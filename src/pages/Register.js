import React, { useState, useContext } from "react";
import {
  Heading,
  Input,
  Stack,
  InputGroup,
  InputLeftAddon,
  Button,
} from "@chakra-ui/core";
import { observer } from "mobx-react";
import { StoreContext } from "../store/store";
import { useHistory } from "react-router-dom";

import InfoBanner from "../components/InfoBanner";
import ErrorBanner from "../components/ErrorBanner";

import { registerUser, getLogin, getUser } from "../api/api";

const Register = observer(() => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [apiError, setApiError] = useState({
    show: false,
    error: {},
  });
  const [apiResponse, setApiResponse] = useState({
    show: false,
    message: "",
  });

  const store = useContext(StoreContext);

  let history = useHistory();

  const performRegister = () => {
    registerUser(username, password).then((response) => {
      if (!response) {
        return;
      }
      if ("error_message" in response) {
        setApiError({
          show: true,
          error: response,
        });
        return;
      }

      setApiResponse({
        show: true,
        message: response?.message,
      });

      getLogin(username, password).then((response) => {
        if (!response) {
          return;
        }
        if ("error_message" in response) {
          setApiError({
            show: true,
            error: response,
          });
          return;
        }
        getUser().then((user) => {
          if ("error_message" in user) {
            return;
          }
          store.user = user;
          history.push("/feed");
        });
      });
    });
  };

  return (
    <div>
      <Heading>Register</Heading>
      <br />
      <br />

      <ErrorBanner apiError={apiError} />
      <InfoBanner show={apiResponse.show} message={apiResponse.message} />

      <Stack spacing={4}>
        <InputGroup>
          <InputLeftAddon children="Username" />
          <Input
            type="text"
            borderLeftRadius="0"
            placeholder="pahagwl"
            value={username}
            onChange={(event) => {
              setUsername(event.target.value);
            }}
          />
        </InputGroup>
        <InputGroup>
          <InputLeftAddon children="Password" />
          <Input
            type="password"
            borderLeftRadius="0"
            placeholder="admin123"
            value={password}
            onChange={(event) => {
              setPassword(event.target.value);
            }}
          />
        </InputGroup>
      </Stack>

      <br />
      <Button colorScheme="blue" onClick={performRegister}>
        Register
      </Button>
    </div>
  );
});

export default Register;
