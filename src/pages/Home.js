import React, { useState, useEffect } from "react";
import { Heading, Box } from "@chakra-ui/core";
import { getAllFaps } from "../api/api";
import FapList from "../components/FapList";

function Home() {
  const [faps, setFaps] = useState([]);

  useEffect(() => {
    getAllFaps().then((response) => {
      if (!response) {
        return;
      }
      if ("error_message" in response) {
        return;
      }
      setFaps(response);
    });
  }, []);

  return (
    <div>
      <br />
      <br />
        <Heading as="h1" size="2xl">Fap to what the world is Fapping to</Heading>
        <br /> <br />

      <Box
        fontWeight="semibold"
        letterSpacing="wide"
        textAlign="center"
        textTransform="uppercase"
        marginBottom="2.5rem"
      >
        Recent Faps
      </Box>

      <FapList faps={faps} />
    </div>
  );
}

export default Home;
