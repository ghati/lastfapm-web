import React, { useState, useContext } from "react";
import {
  Heading,
  Input,
  Stack,
  InputGroup,
  InputLeftAddon,
  Button,
} from "@chakra-ui/core";
import { observer } from "mobx-react";
import { StoreContext } from "../store/store";
import { useHistory } from "react-router-dom";

import ErrorBanner from "../components/ErrorBanner";

import { getLogin, getUser } from "../api/api";

const Login = observer(() => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [apiError, setApiError] = useState({
    show: false,
    error: {},
  });

  const store = useContext(StoreContext);

  let history = useHistory();

  const performLogin = () => {
    getLogin(username, password).then((response) => {
      if (!response) {
        return;
      }
      if ("error_message" in response) {
        setApiError({
          show: true,
          error: response,
        });
        return;
      }
      getUser().then((user) => {
        if ("error_message" in user) {
          return;
        }
        store.user = user;
        history.push("/feed");
      });
    });
  };

  return (
    <div>
      <Heading>Login</Heading>
      <br />
      <br />

      <ErrorBanner apiError={apiError} />

      <Stack spacing={4}>
        <InputGroup>
          <InputLeftAddon children="Username" />
          <Input
            type="text"
            borderLeftRadius="0"
            placeholder="pahagwl"
            value={username}
            onChange={(event) => {
              setUsername(event.target.value);
            }}
          />
        </InputGroup>
        <InputGroup>
          <InputLeftAddon children="Password" />
          <Input
            type="password"
            borderLeftRadius="0"
            placeholder="admin123"
            value={password}
            onChange={(event) => {
              setPassword(event.target.value);
            }}
          />
        </InputGroup>
      </Stack>

      <br />
      <Button colorScheme="blue" onClick={performLogin}>
        Login
      </Button>
    </div>
  );
});

export default Login;
