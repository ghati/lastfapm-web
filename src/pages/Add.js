import React, { useState } from "react";
import {
  Heading,
  Input,
  Stack,
  InputGroup,
  InputLeftAddon,
  Button,
} from "@chakra-ui/core";

import ErrorBanner from "../components/ErrorBanner";
import InfoBanner from "../components/InfoBanner";

import { addFap } from "../api/api";

const Add = () => {
  const [title, setTitle] = useState("");

  const [apiError, setApiError] = useState({
    show: false,
    error: {},
  });

  const [apiResponse, setApiResponse] = useState({
    show: false,
    message: "",
  });

  const submitFap = () => {
    let fapType = "new";
    let fapContent = {
      title: title,
    };

    addFap(fapType, fapContent).then((response) => {
      if (!response) {
        return;
      }
      if ("error_message" in response) {
        setApiError({
          show: true,
          error: response,
        });

        return;
      }

      setApiResponse({
        show: true,
        message: response?.message,
      });
    });
  };

  return (
    <div>
      <Heading>Add a Fap</Heading>
      <br />

      <InfoBanner show={apiResponse.show} message={apiResponse.message} />
      <ErrorBanner apiError={apiError} />

      <Stack spacing={4}>
        <InputGroup>
          <InputLeftAddon children="Content Title" />
          <Input
            type="text"
            borderLeftRadius="0"
            placeholder="Savita Bhabhi Volume 3 HD-RIP"
            value={title}
            onChange={(event) => {
              setTitle(event.target.value);
            }}
          />
        </InputGroup>
      </Stack>

      <br />
      <Button colorScheme="blue" onClick={submitFap}>
        Submit
      </Button>
    </div>
  );
};

export default Add;
