import React, { useEffect, useState } from "react";
import { Heading } from "@chakra-ui/core";
import { useParams } from "react-router";
import { getFapsByUsername } from "../api/api";
import { DateTime } from "luxon";
import ErrorBanner from "../components/ErrorBanner";
import FapList from "../components/FapList";

const UserCard = ({ user, faps }) => {
  if (!user) {
    return null;
  }

  return (
    <div style={{ marginBottom: "2rem" }}>
      <center>
        <Heading as="h1" size="2xl">
          {user?.username}
        </Heading>
        <Heading as="h5" size="sm">
          last fapped {DateTime.fromISO(faps?.[0].created_at).toRelative()}{" "}
          <br />
          joined {DateTime.fromISO(user?.created_at).toRelative()}
        </Heading>
      </center>
    </div>
  );
};

const Profile = () => {
  let { username } = useParams();

  const [apiResponse, setApiResponse] = useState({});

  const [apiError, setApiError] = useState({
    show: false,
    error: {},
  });

  useEffect(() => {
    getFapsByUsername(username).then((response) => {
      if (!response) {
        return;
      }
      if ("error_message" in response) {
        setApiError({
          show: true,
          error: response,
        });
        return;
      }
      setApiResponse(response);
    });
  }, [username]);

  return (
    <div>
      <ErrorBanner apiError={apiError} />
      <UserCard user={apiResponse?.user} faps={apiResponse?.faps} />
      <FapList faps={apiResponse?.faps} />
    </div>
  );
};

export default Profile;
