import React from "react";
import { Box, Text } from "@chakra-ui/core";
import { DateTime } from "luxon";
import { Link } from "react-router-dom";

const FapListing = ({ fap }) => {
  return (
    <div>
      <Box borderWidth="1px" rounded="lg" overflow="hidden">
        <Box p="6">
          <Text fontSize="xl" fontWeight="semibold">
            {fap?.content?.title}
          </Text>
          <Text color="gray.300">
            <Link to={"/profile/" + fap?.username}>{fap?.username}</Link>
          </Text>
          <Text
            color="gray.500"
            fontWeight="semibold"
            letterSpacing="wide"
            fontSize="xs"
            textTransform="uppercase"
          >
            {DateTime.fromISO(fap?.created_at).toRelative()}
          </Text>
        </Box>
      </Box>
      <br />
    </div>
  );
};

const FapList = ({ faps }) => {
  let elements = [];

  if (!faps) {
    return null;
  }

  faps.map((fap, index) => {
    return elements.push(<FapListing key={index} fap={fap} />);
  });

  return elements;
};

export default FapList;
