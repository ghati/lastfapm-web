import React from "react";
import { Alert, AlertDescription } from "@chakra-ui/core";

const InfoBanner = ({ show, message }) => {
  if (show) {
    return (
      <>
        <Alert status="info">
          <AlertDescription>{message}</AlertDescription>
        </Alert>
        <br />
      </>
    );
  }

  return null;
};

export default InfoBanner;
