import React, { useContext } from "react";
import { Box, Heading, Flex, Text, Button } from "@chakra-ui/core";
import { Link } from "react-router-dom";
import { observer } from "mobx-react";
import { StoreContext } from "../store/store";
import { useHistory } from "react-router-dom";

const MenuItems = ({ children }) => (
  <Text mt={{ base: 4, md: 0 }} mr={6} display="block">
    {children}
  </Text>
);

const ShowSessionButton = observer(() => {
  const store = useContext(StoreContext);

  let history = useHistory();

  const logoutUser = () => {
    console.log("Logging out");
    document.cookie = "lfpm-session=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    history.push("/");
    store.user = {};
  };

  if (store.user.username) {
    return (
      <Button bg="transparent" border="1px" onClick={logoutUser}>
        Logout
      </Button>
    );
  } else {
    return (
      <>
        <Link to="/login" style={{ paddingRight: "15px" }}>
          <Button bg="transparent" border="1px">
            Login
          </Button>
        </Link>
        <Link to="/register">
          <Button bg="transparent" border="1px">
            Register
          </Button>
        </Link>
      </>
    );
  }
});

const LinksBehindLogin = observer(() => {
  const store = useContext(StoreContext);

  if (store.user.username) {
    return (
      <>
        <MenuItems>
          <Link to={"/profile/" + store.user.username}>Profile</Link>
        </MenuItems>
        <MenuItems>
          <Link to={"/add"}>Add</Link>
        </MenuItems>
      </>
    );
  } else {
    return null;
  }
});

function Header(props) {
  const [show, setShow] = React.useState(false);
  const handleToggle = () => setShow(!show);

  return (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      wrap="wrap"
      padding="1.5rem"
      bg="orange.500"
      color="white"
      marginBottom="2.5rem"
      {...props}
    >
      <Flex align="center" mr={5}>
        <Heading as="h1" size="lg" letterSpacing={"-.1rem"}>
          <Link to="/">LastFapM</Link>
        </Heading>
      </Flex>

      <Box display={{ base: "block", md: "none" }} onClick={handleToggle}>
        <svg
          fill="white"
          width="12px"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <title>Menu</title>
          <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
        </svg>
      </Box>

      <Box
        display={{ sm: show ? "block" : "none", md: "flex" }}
        width={{ sm: "full", md: "auto" }}
        alignItems="center"
        flexGrow={1}
      >
        <LinksBehindLogin />
      </Box>

      <Box
        display={{ sm: show ? "block" : "none", md: "block" }}
        mt={{ base: 4, md: 0 }}
      >
        <ShowSessionButton />
      </Box>
    </Flex>
  );
}

export default Header;
