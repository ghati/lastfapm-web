import React from "react";
import { Alert, AlertTitle, AlertDescription } from "@chakra-ui/core";

const ErrorBanner = ({ apiError }) => {
  if (apiError.show) {
    return (
      <>
        <Alert status="error">
          <AlertTitle mr={2}>Haaaaat!</AlertTitle>
          <AlertDescription>{apiError.error.error_message}</AlertDescription>
        </Alert>
        <br />
      </>
    );
  }

  return null;
};

export default ErrorBanner;
