import { useEffect, useContext } from "react";
import { getUser } from "../api/api";
import { observer } from "mobx-react";
import { StoreContext } from "../store/store";

const LoadUser = observer(() => {
  const store = useContext(StoreContext);
  useEffect(() => {
    getUser().then((user) => {
      if (!user) {
        return;
      }
      if ("error_message" in user) {
        return;
      }
      store.user = user;
    });
  });
  return null;
});

export default LoadUser;
