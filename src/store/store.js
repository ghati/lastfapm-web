import { observable, decorate } from "mobx";
import { createContext } from "react";

class Store {
  user = {};
}

decorate(Store, {
  user: observable,
});

export const StoreContext = createContext(new Store());
