import { extendTheme } from "@chakra-ui/core";

const fonts = {
  heading: '"Avenir Next", sans-serif',
  body: "system-ui, sans-serif",
  mono: "Menlo, monospace",
};

const customTheme = extendTheme({
  config: {
    useSystemColorMode: "true",
    initialColorMode: "dark",
  },
  fonts,
});

export default customTheme;
